<?php

namespace Database\Factories;

use App\Models\Lesson;
use Illuminate\Database\Eloquent\Factories\Factory;

class LessonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'lesson_fee' => $this->faker->numberBetween(10000),
            'is_enable' => Lesson::STATUS_ENABLE
        ];
    }
}
