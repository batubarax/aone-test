<?php

namespace Database\Seeders;

use App\Models\Lesson;
use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentLessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::factory()
            ->count(50)
            ->has(Lesson::factory()->count(10), 'lessonStudents')
            ->create();
    }
}
