<?php

namespace App\Console\Commands;

use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Lesson;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PopulateInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:populate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate invoice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Populate Invoice Begin');
        DB::beginTransaction();
        try {
            Student::with('lessonStudents')->where([
                'invoice_day' => Carbon::now()->day
            ])
                ->whereHas('lessonStudents', function ($query) {
                    return $query->where('is_enable', Lesson::STATUS_ENABLE);
                })
                ->where('is_active', Student::STATUS_ACTIVE)
                ->chunk(2000, function ($studentPaydays) {

                    foreach ($studentPaydays as $studentPayday) {
                        if ($studentPayday->lessonStudents->count() == 0) {
                            continue;
                        }

                        $invoice = new Invoice();
                        $invoice->student_id = $studentPayday->id;
                        $invoice->total_amount = $studentPayday->lessonStudents->sum('lesson_fee');
                        $invoice->invoice_date = Carbon::now();
                        $invoice->save();

                        $invoiceDetails = [];
                        foreach ($studentPayday->lessonStudents as $lesson) {
                            $invoiceDetails[] = [
                                'lesson_id' => $lesson->id,
                                'lesson_fee' => $lesson->lesson_fee
                            ];
                        }

                        $invoice->invoiceDetails()->createMany($invoiceDetails);

                        DB::commit();
                    }
                });
            $this->info('Populate Invoice Done');
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}
