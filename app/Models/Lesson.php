<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;

    public const STATUS_ENABLE = 1;
    public const STATUS_DISABLE = 0;

    protected $fillable = [
        'name',
        'lesson_fee',
        'is_enable'
    ];

    public function lessonStudent()
    {
        return $this->belongsToMany(Student::class, 'lesson_student');
    }
}
