<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    public const STATUS_ACTIVE = 1;
    public const STATUS_NONACTIVE = 0;

    protected $fillable = [
        'name',
        'email',
        'invoice_day',
        'is_active'
    ];

    public function lessonStudents()
    {
        return $this->belongsToMany(Lesson::class, 'lesson_students');
    }
}
